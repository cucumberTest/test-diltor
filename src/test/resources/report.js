$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("app.feature");
formatter.feature({
  "line": 1,
  "name": "the answer is randomly \u0027Yes\u0027 or \u0027No\u0027",
  "description": "",
  "id": "the-answer-is-randomly-\u0027yes\u0027-or-\u0027no\u0027",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "client makes call to GET /greeting",
  "description": "",
  "id": "the-answer-is-randomly-\u0027yes\u0027-or-\u0027no\u0027;client-makes-call-to-get-/greeting",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "the Spring Boot Application is running",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "the client calls /greeting",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "the client receives status code of 200",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "the client receives Yes or No",
  "keyword": "And "
});
formatter.match({
  "location": "ResponseResults.theSpringBootApplicationIsRunning()"
});
formatter.result({
  "duration": 5025607512,
  "status": "passed"
});
formatter.match({
  "location": "ResponseResults.theClientCallsGreeting()"
});
formatter.result({
  "duration": 388989508,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 35
    }
  ],
  "location": "ResponseResults.theClientReceivesStatusCodeOf(int)"
});
formatter.result({
  "duration": 4213639,
  "status": "passed"
});
formatter.match({
  "location": "ResponseResults.theClientReceivesYesOrNo()"
});
formatter.result({
  "duration": 1592451,
  "error_message": "cucumber.api.PendingException: TODO: implement me\r\n\tat hello.ResponseResults.theClientReceivesYesOrNo(ResponseResults.java:42)\r\n\tat ✽.And the client receives Yes or No(app.feature:6)\r\n",
  "status": "pending"
});
});