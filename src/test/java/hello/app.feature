Feature: the answer is randomly 'Yes' or 'No'
  Scenario: client makes call to GET /greeting
    Given the Spring Boot Application is running
    When the client calls /greeting
    Then the client receives status code of 200
    And the client receives Yes or No