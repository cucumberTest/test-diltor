package hello;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/hello", plugin = {"pretty" ,"html:src/test/resources" ,
"json:src/test/resources/cucumber.json"})
public class GreetingControllerTest{

}