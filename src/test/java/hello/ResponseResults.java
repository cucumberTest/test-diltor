package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.web.client.RestTemplate;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class ResponseResults {

    Greeting webServiceResponse;
    RestTemplate template = new RestTemplate();
    private final String homeUrl = "http://localhost:8080";

    @Given("^the Spring Boot Application is running$")
    public void theSpringBootApplicationIsRunning() throws Throwable {
        SpringApplication.run(Application.class);
    }

    @When("^the client calls /greeting$")
    public void theClientCallsGreeting() throws Throwable {
        //final URL url = new URL(homeUrl + "/greeting");
        webServiceResponse = template.getForObject(homeUrl + "/greeting", Greeting.class);
        System.out.println(webServiceResponse.getStatus());
        System.out.println(webServiceResponse.getContent());
    }

    @Then("^the client receives status code of (\\d+)$")
    public void theClientReceivesStatusCodeOf(final int statusCode) throws Throwable {


    }

    @And("^the client receives Yes or No$")
    public void theClientReceivesYesOrNo() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


}
