package hello;



public class Greeting {

    private int status;

    private String content;

    public Greeting() {

    }

    public Greeting(final int status, final String content) {
        this.status = status;
        this.content = content;
    }

    public String getContent() {
        return content;
    }
    public int getStatus(){ return status; }
}