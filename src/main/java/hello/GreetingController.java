package hello;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "%s";

    @RequestMapping(value = "/greeting",produces= MediaType.APPLICATION_JSON_VALUE)
    public static Greeting greeting() {
        int randNumber = (int) Math.round( Math.random());
        String answer = "Yes";

        if(randNumber == 0){
            answer = "No";
        }
        return new Greeting(200,String.format(template, answer));
    }
}